#!/usr/bin/python

"""
General development profile for rooftop base stations and fixed endpoints.

Default images use low-latency kernels.
"""

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab.pnext as pn
import geni.rspec.igext as ig
import geni.rspec.emulab.spectrum as spectrum


DEFAULT_X310_NODE_IMAGE = "urn:publicid:IDN+emulab.net+image+PowderProfiles:ubuntu1804lowlatency"
DEFAULT_B210_NODE_IMAGE = "urn:publicid:IDN+emulab.net+image+PowderProfiles:ubuntu1804lowlatency"
DEFAULT_NODE_IMAGE = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"

node_type = [
    ("d740", "Emulab - d740"),
    ("d430", "Emulab - d430"),
]

ROOFTOP_NAMES = [
    ("cellsdr1-bes", "Emulab - cellsdr1-bes (Behavioral)"),
    ("cellsdr1-browning", "Emulab - cellsdr1-browning (Browning)"),
    ("cellsdr1-dentistry", "Emulab - cellsdr1-dentistry (Dentistry)"),
    ("cellsdr1-fm", "Emulab - cellsdrsdr1-fm (Friendship Manor)"),
    ("cellsdr1-honors", "Emulab - cellsdrs1-honors (Honors)"),
    ("cellsdr1-meb", "Emulab - cellsdr1-meb (MEB)"),
    ("cellsdr1-smt", "Emulab - cellsdrsdr1-smt (SMT)"),
    ("cellsdr1-ustar", "Emulab - cellsdr1-ustar (USTAR)"),
]

FIXED_ENDPOINT_AGGREGATES = [
    ("web", "WEB - nuc2"),
    ("ebc", "EBC - nuc2"),
    ("bookstore", "Bookstore - nuc2"),
    ("humanities", "Humanities - nuc2"),
    ("law73", "Law 73 - nuc2"),
    ("madsen", "Madsen - nuc2"),
    ("sagepoint", "Sage Point - nuc2"),
    ("moran", "Moran - nuc2"),
]


def x310_node_pair(idx, x310_node, node_type, x310_node_image):
    radio_link = request.Link("radio-link-%d" % idx)
    radio_link.bandwidth = 10 * 1000 * 1000

    node = request.RawPC("%s-comp" % x310_node.radio_name)
    node.hardware_type = node_type
    node.disk_image = x310_node_image
    node.component_manager_id = "urn:publicid:IDN+emulab.net+authority+cm"

    node_radio_if = node.addInterface("usrp_if")
    node_radio_if.addAddress(rspec.IPv4Address("192.168.40.1",
                                               "255.255.255.0"))
    radio_link.addInterface(node_radio_if)

    radio = request.RawPC("%s-x310" % x310_node.radio_name)
    radio.component_id = x310_node.radio_name
    radio_link.addNode(radio)


def b210_nuc_pair(b210_node, b210_node_image):
    b210_nuc_pair_node = request.RawPC("b210-%s-%s" % (b210_node.aggregate_id, "nuc2"))
    agg_full_name = "urn:publicid:IDN+%s.powderwireless.net+authority+cm" % (b210_node.aggregate_id)
    b210_nuc_pair_node.component_manager_id = agg_full_name
    b210_nuc_pair_node.component_id = "nuc2"
    b210_nuc_pair_node.disk_image = b210_node_image


def raw_pc(node_num, node_type, general_node_image):
    node = request.RawPC("compute%s" % node_num)
    node.hardware_type = node_type
    node.disk_image = general_node_image


portal.context.defineParameter("x310_node_image", "X310 node image",
                               portal.ParameterType.STRING, DEFAULT_X310_NODE_IMAGE)

portal.context.defineParameter("b210_node_image", "B210 node image",
                               portal.ParameterType.STRING, DEFAULT_B210_NODE_IMAGE)

portal.context.defineParameter("general_node_image", "Image for other compute",
                               portal.ParameterType.STRING, DEFAULT_NODE_IMAGE)

portal.context.defineParameter("x310_pair_nodetype",
                               "Type of compute node paired with the X310 Radios",
                               portal.ParameterType.STRING, node_type[0], node_type)

portal.context.defineParameter("compute_nodetype",
                               "Type of node for other compute",
                               portal.ParameterType.STRING, node_type[0], node_type)

portal.context.defineStructParameter("x310_nodes", "X310 Radios", [],
                                     multiValue=True, itemDefaultValue={},
                                     min=0, max=None,
                                     members=[portal.Parameter(
                                         "radio_name",
                                         "Rooftop base-station X310",
                                         portal.ParameterType.STRING,
                                         ROOFTOP_NAMES[0],
                                         ROOFTOP_NAMES
                                     )])

portal.context.defineStructParameter("b210_nodes", "B210 Radios", [],
                                     multiValue=True, min=0, max=None,
                                     members=[portal.Parameter(
                                         "aggregate_id",
                                         "Fixed Endpoint B210",
                                         portal.ParameterType.STRING,
                                         FIXED_ENDPOINT_AGGREGATES[0],
                                         FIXED_ENDPOINT_AGGREGATES
                                     )])

portal.context.defineParameter("num_compute_nodes", "Number of extra compute nodes",
                               portal.ParameterType.INTEGER, 0, min=0, max=3)

params = portal.context.bindParameters()
request = portal.context.makeRequestRSpec()

request.requestSpectrum(2560, 2570, 0)
request.requestSpectrum(2680, 2690, 0)

for i, x310_node in enumerate(params.x310_nodes):
    x310_node_pair(i, x310_node, params.x310_pair_nodetype, params.x310_node_image)

for b210_node in params.b210_nodes:
    b210_nuc_pair(b210_node, params.b210_node_image)

for i in range(params.num_compute_nodes):
    raw_pc(i, params.compute_nodetype, params.general_node_image)

portal.context.printRequestRSpec()
